#include "lexer.hpp"
#include "context.hpp"

#include <iostream>
#include <fstream>
#include <sstream>

namespace sexpr
{
  [[noreturn]] 
  static void 
  throw_invalid_character_err(cc::location loc, char c)
  {
    std::stringstream ss;
    ss << "invalid character '" << c << '\'';
    throw lexical_error(loc, ss.str());
  }

  [[noreturn]]
  static void
  throw_invalid_number_err(cc::location loc, const std::string& str)
  {
    std::stringstream ss;
    ss << "invalid number '" << str << '\'';
    throw lexical_error(loc, ss.str());
  }

  lexer::lexer(context& cxt, const cc::file& f)
    : cxt(cxt), first(f.begin()), limit(f.end()), off(f.get_base_offset())
  { }

  /// Consume a single character and return its location.
  cc::location
  lexer::consume()
  {
    ++first;
    return cc::location(off++); 
  }

  /// Consume white space.
  void
  lexer::space()
  {
    while (first != limit && std::isspace(*first))
      consume();
  }

  /// Consume comments.
  ///
  /// FIXME: Save comments in the lexical context.
  void
  lexer::comment()
  {
    while (first != limit && *first != '\n')
      consume();
  }

  token
  lexer::next() 
  {
    while (first != limit) {
      switch (*first) {
        case ' ':
        case '\t':
        case '\n':
        case '\r':
          space();
          continue;

        case ';':
          comment();
          continue;

        case '(':
          return punctuator(tk_lparen);
        
        case ')':
          return punctuator(tk_rparen);

        case '"':
          return string();

        default: {
          if (*first == '-' || std::isdigit(*first))
            return number();
          if (std::isgraph(*first))
            return identifier();
          
          char c = *first;
          cc::location loc = consume();
          throw_invalid_character_err(loc, c);
        }
      }
    }
    return {cc::location(off), tk_eof};
  }


  /// Matches and returns a new punctuator token.
  token
  lexer::punctuator(token_kind k)
  {
    cc::location loc = consume();
    return {loc, k};
  }

  /// Matches and returns a numeric token.
  ///
  /// \todo Extend this to support floating point numbers.
  token
  lexer::number()
  {
    cc::location loc(off);
    const char* iter = first;
    consume();
    while (first != limit) {
      if (std::isspace(*first))
        break;
      if (*first == ')')
        break;
      consume();
    }
    
    std::string str(iter, first);
    
    // Interpret the string an an integer value.
    std::size_t pos;
    int val = std::stoi(str, &pos, 0);
    if (pos != str.size())
      throw_invalid_number_err(loc, str);

    const std::string* sym = cxt.get_symbol(str);
    return {loc, tk_int, sym, val};
  }

  // Matches and returns a string token.
  token
  lexer::string()
  {
    cc::location loc(off);
    const char* iter = first++;
    consume();
    while (first != limit) {
      if (*first == '"') {
        consume();
        if (first == limit || *first != '"')
          break;
      }
      consume();
    }

    if (first == limit)
      throw lexical_error(loc, "unterminated string");

    std::string str(iter, first);
    const std::string* sym = cxt.get_symbol(str);
    return {loc, tk_id, sym};
  }

  /// Matches and returns an identifier token.
  token
  lexer::identifier()
  {
    cc::location loc(off);
    const char* iter = first++;
    while (first != limit) {
      if (std::isspace(*first))
        break;
      if (*first == ')')
        break;
      consume();
    }

    std::string str(iter, first);
    const std::string* sym = cxt.get_symbol(str);
    return {loc, tk_id, sym};
  }
} // namespace sexpr
