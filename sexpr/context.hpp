#pragma once

#include <cc/diagnostics.hpp>
#include <cc/factory.hpp>
#include <cc/input.hpp>
#include <cc/symbol.hpp>

namespace sexpr
{

  struct expr;
  struct nil_expr;
  struct id_expr;
  struct int_expr;
  struct str_expr;
  struct list_expr;

  using expr_seq = std::vector<expr*>;

// -------------------------------------------------------------------------- //
// Expression factory

  /// An expression factory owns the objects that are allocated through it.
  ///
  /// This is used by the ast context to manage allocations. Other instances
  /// of expression factories can be created to make short-lived expressions
  /// for analyses that require them.
  ///
  /// \todo Actually give this an interface.
  struct expr_factory
  {
    cc::basic_factory<nil_expr> nils;
    cc::basic_factory<id_expr> ids;
    cc::basic_factory<int_expr> ints;
    cc::basic_factory<str_expr> strs;
    cc::basic_factory<list_expr> lists;
  };

// -------------------------------------------------------------------------- //
// Syntactic context

  /// The context provides access to a number of useful translation facilities
  /// and manages memory for expressions.
  class context : private expr_factory
  {
  public:
    context(cc::diagnostic_manager& diags, 
            cc::input_manager& in, 
            cc::symbol_table& syms);

    /// Returns the diagnostic manager.
    cc::diagnostic_manager& get_diagnostics() { return diags; }

    /// Returns the input manager.
    cc::input_manager& get_input() const { return input; }

    /// Returns the associated lexical context.
    cc::symbol_table& get_symbol_table() { return syms; }

    /// Returns a unique symbol.
    cc::symbol* get_symbol(const char* str);
    cc::symbol* get_symbol(const std::string& str);

    /// Returns a new nil node.
    expr* make_nil_expr(cc::span locs);

    /// Returns a new identifier node. 
    expr* make_id_expr(cc::span locs, cc::symbol* sym);

    /// Returns a new integer node.
    expr* make_int_expr(cc::span locs, int val);

    /// Returns a new string node.
    expr* make_str_expr(cc::span locs, cc::symbol* sym);

    /// Returns a new list node.
    expr* make_list_expr(cc::span locs, expr_seq&& nodes);

  private:
    /// The diagnostic manager.
    cc::diagnostic_manager& diags;

    /// The input manager.
    cc::input_manager& input;

    /// The symbol table.
    cc::symbol_table& syms;
  };

} // namesapce sexpr
