#include "syntax.hpp"
#include "dumper.hpp"

#include <cassert>
#include <sstream>

namespace sexpr
{
  const char*
  get_expr_name(expr_kind k) 
  {
    switch (k) {
      case ek_nil:
        return "nil";
      case ek_id:
        return "id";
      case ek_int:
        return "int";
      case ek_str:
        return "str";
      case ek_list:
        return "list";
      default:
        break;
    }
    throw std::logic_error("invalid node kind");
  }

  void
  expr::dump() const
  {
    sexpr::dump(this);
  }

} // namespace sexpr
