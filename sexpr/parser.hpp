#pragma once

#include <sexpr/lexer.hpp>

#include <functional>
#include <unordered_map>

namespace sexpr
{
  class expr;
  class context;

  /// The sexpr parser. This recognizes sequences of enclosed atoms 
  /// (identifiers and literals) and lists. 
  class parser
  {
  public:
    parser(context& cxt, const cc::file& f);

    expr* operator()();

    /// Returns the lexer.
    lexer& get_lexer() { return lex; }

  private:
    friend class balanced_parens;

    // Tokens
    bool next_token_is(token_kind k);
    bool next_token_is_not(token_kind k);
    token consume();
    token match(token_kind k);

    // Productions
    expr* parse_expr();
    expr* parse_list();
    expr* parse_identifier();
    expr* parse_string();
    expr* parse_integer();

  private:
    /// The AST context.
    context& cxt;

    /// The lexer.
    lexer lex;

    /// The lookahead token.
    token tok;
  };


  /// Represents a parsing error.
  class syntax_error : public cc::diagnosable_error
  {
  public:
    syntax_error(cc::location loc, const std::string& msg)
      : cc::diagnosable_error(cc::dk_error, "syntax", loc, msg)
    { }
  };

} // namespace sexpr
