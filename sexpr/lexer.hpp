#pragma once

#include <sexpr/token.hpp>

#include <cc/symbol.hpp>
#include <cc/file.hpp>
#include <cc/diagnostics.hpp>

namespace sexpr
{
  class context;

  /// A small machine that transforms characters into tokens.
  ///
  /// \todo Extract comments and maintain them separately.
  class lexer
  {
  public:
    lexer(context& cxt, const cc::file& f);

    token operator()() { return next(); }

  private:
    cc::location consume();

    void space();
    void comment();

    token next();
    token punctuator(token_kind k);
    token number();
    token string();
    token identifier();

    /// A global symbol table.
    context& cxt;

    /// Iterators into the current buffer.
    const char* first;
    const char* limit;

    /// The current offset.
    std::size_t off;
  };


  /// Represents a lexical error.
  class lexical_error : public cc::diagnosable_error
  {
  public:
    lexical_error(cc::location loc, const std::string& msg)
      : cc::diagnosable_error(cc::dk_error, "lexical", loc, msg)
    { }
  };

} // namespace sexpr
