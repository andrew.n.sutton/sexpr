#pragma once

#include <cc/symbol.hpp>
#include <cc/bytecode.hpp>

#include <unordered_map>
#include <vector>

namespace sexpr
{

class context;
struct expr;
struct nil_expr;
struct id_expr;
struct int_expr;
struct str_expr;
struct list_expr;

struct output_record
{
  output_record()
    : bytes(), os(bytes)
  { }
  
  cc::byte_seq bytes;
  cc::byte_ostream os;
};

/// A list of T elements tobe serialized accompanied by a map to
/// ensure the uniqueness of serialized terms.
template<typename T>
struct lookup_table
{
  int add(T t)
  {
    auto result = map.emplace(t, seq.size());
    if (result.second)
      seq.push_back(t);
    return result.first->second;
  }

  std::vector<T> seq;
  std::unordered_map<T, int> map;
};

/// Stores a list of serialized sections.
struct section_table : std::vector<cc::byte_seq>
{
  using std::vector<cc::byte_seq>::vector;
};

class bytecode_writer
{
public:
  bytecode_writer(context& cxt)
    : cxt(cxt)
  { }

  void operator()(const expr* e, const std::string& f);

  void write_symbol(const cc::symbol* s);

  void write_expr(const expr* e);
  void write_nil_expr(const nil_expr* e);
  void write_id_expr(const id_expr* e);
  void write_int_expr(const int_expr* e);
  void write_str_expr(const str_expr* e);
  void write_list_expr(const list_expr* e);

private:
  /// A unique lookup table for symbols.
  lookup_table<cc::symbol*> syms;

  /// The main serialized content of an expression.
  output_record exprs;

  /// Facilities.
  context& cxt;
};

class bytecode_reader
{
public:
  bytecode_reader(context& cxt)
    : cxt(cxt)
  { }

  const expr* read_expr();
  const nil_expr* read_nil_expr();
  const id_expr* read_id_expr();
  const int_expr* read_int_expr();
  const str_expr* read_str_expr();
  const list_expr* read_list_expr();

private:
  context& cxt;
};

} // namespace sexpr
