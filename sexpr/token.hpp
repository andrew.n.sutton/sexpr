#pragma once

#include <cc/symbol.hpp>
#include <cc/location.hpp>

#include <cassert>
#include <iosfwd>
#include <string>

namespace sexpr
{

  /// Kinds (or classes) of tokens recognized by the lexer.
  enum token_kind
  {
    /// The null token.
    tk_eof,

    tk_lparen,
    tk_rparen,
    tk_id,
    tk_int,
    tk_str,
  };


  /// An abstract symbol in the language. Tokens are produced by the
  /// lexer.
  class token
  {
  public:
    token()
      : loc(), kind(tk_eof), sym()
    { }

    token(cc::location l, int k)
      : loc(l), kind(k), sym()
    { }

    token(cc::location l, int k, cc::symbol* s)
      : loc(l), kind(k), sym(s)
    { }

    token(cc::location l, int k, cc::symbol* s, int a)
    : loc(l), kind(k), sym(s), attr(a)
  { }

    /// Converts to true if the token is not end-of-file.
    explicit operator bool() const { return kind != tk_eof; }

    /// Returns the name of the token class.
    const char* get_name() const { return get_name(kind); }

    /// Returns the name of the token class.
    static const char* get_name(int k);

    /// Returns the (start) location of a token.
    cc::location get_location() const { return loc; }

    /// Returns the start location of a token.
    cc::location get_start_location() const { return loc; }

    /// Returns the end location of a token.
    cc::location get_end_location() const;

    /// Returns the span of the token.
    cc::span get_span() const 
    { 
      return {get_start_location(), get_end_location()}; 
    }

    /// Returns the kind of token.
    int get_kind() const { return kind; }

    /// True if this is an end-of-file token.
    bool is_eof() const { return kind == tk_eof; }

    /// Returns the spelling of the token.
    const char* get_spelling() const;

    /// Returns the symbol of the token if it has a distinct spelling.
    cc::symbol* get_symbol() const { return sym; }

    /// Returns the spelling of the token's symbol.
    const std::string& get_string() const;

    /// If the token was an integer value, get that.
    int get_integer() const;
  
  private:
    /// The starting location of the token.
    cc::location loc;

    /// The kind or class of token.
    int kind;

    /// The token's spelling if non-trivial.
    cc::symbol* sym;

    /// An additional attribute that stores extra information
    /// about the token (e.g., its integer interpretation).
    ///
    /// \todo This should be union, discriminate by kind.
    int attr; 
  };

  inline const std::string& 
  token::get_string() const
  {
    assert(kind >= tk_id);
    return *sym;
  }

  inline int
  token::get_integer() const
  {
    assert(kind == tk_int);
    return attr;
  }

  std::ostream& operator<<(std::ostream& os, const token& tok);

} // namespace sexpr
