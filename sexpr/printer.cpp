#include "printer.hpp"
#include "syntax.hpp"

#include <iostream>

namespace sexpr 
{
  void
  printer::print(const expr* e)
  {
    return print_expr(e);
  }

  void
  printer::print_expr(const expr* e)
  {
    switch (get_node_kind(e)) {
      case ek_nil:
        return print_nil_expr(cast<nil_expr>(e));
      case ek_id:
        return print_id_expr(cast<id_expr>(e));
      case ek_int:
        return print_int_expr(cast<int_expr>(e));
      case ek_list:
        return print_list_expr(cast<list_expr>(e));
      default:
        break;
    }
    throw std::logic_error("invlaid expr");
  }

  void
  printer::print_nil_expr(const nil_expr* e)
  {
    os << "()";
  }

  void
  printer::print_id_expr(const id_expr* e)
  {
    os << *e->id;
  }

  void
  printer::print_int_expr(const int_expr* e)
  {
    os << e->val;
  }

  void
  printer::print_list_expr(const list_expr* e)
  {
    os << '(';
    const expr_seq& exprs = e->exprs;
    for (auto iter = exprs.begin(); iter != exprs.end(); ++iter) {
      print_expr(*iter);
      if (std::next(iter) != exprs.end())
        os << ' ';
    }
    os << ')';
  }

  void
  print(const expr* e)
  {
    printer p(std::cout);
    p.print(e);
    std::cout << '\n';
  }

  std::ostream& 
  operator<<(std::ostream& os, const expr* e)
  {
    printer p(os);
    p.print(e);
    return os;
  }

} // namespace sexpr
