#pragma once

#include <cc/dump.hpp>

namespace sexpr
{
  class expr;
  class nil_expr;
  class id_expr;
  class int_expr;
  class str_expr;
  class list_expr;

  /// The AST dumper is responsible for dumping the internal representation
  /// of a source tree to an output stream.
  class dumper : public cc::dumper
  {
  public:
    using cc::dumper::dumper;

    void operator()(const expr* e) { return dump_expr(e); }

  private:
    void dump_expr(const expr* e);
    void dump_nil_expr(const nil_expr* e);
    void dump_id_expr(const id_expr* e);
    void dump_int_expr(const int_expr* e);
    void dump_str_expr(const str_expr* e);
    void dump_list_expr(const list_expr* e);

    struct dump_guard : cc::dumper::dump_guard
    {
      dump_guard(dumper& d, const expr* e, bool nl = true);
    };
  };

  void dump(const expr* e);

} // namespace sexpr
