#pragma once

#include <sexpr/syntax.hpp>

#include <cc/diagnostics.hpp>

namespace sexpr
{
  struct expr;
  struct nil_expr;
  struct id_expr;
  struct int_expr;
  struct str_expr;
  struct list_expr;

// -------------------------------------------------------------------------- //
// Exceptions

  /// Represents an s-expr translation error.
  class translation_error : public cc::diagnosable_error
  {
  public:
    translation_error(cc::location loc, const std::string& msg)
      : cc::diagnosable_error(cc::dk_error, "translation", loc, msg)
    { }
  };
  
  // Exceptions
  [[noreturn]] void throw_unexpected_term(const expr* e);
  [[noreturn]] void throw_expected_term(const expr* e, const char* what);
  [[noreturn]] void throw_unexpected_id(const id_expr* e);
  [[noreturn]] void throw_expected_id(const id_expr* e, const char* what);
  [[noreturn]] void throw_expected_id(const expr* e);
  [[noreturn]] void throw_expected_value(const int_expr* e, int what);


// -------------------------------------------------------------------------- //
// Translation

  /// This class provides matching facilities for derived classes.
  template<typename Derived>
  struct translator
  {
    /// Returns the derived translator.
    const Derived& derived() const { return static_cast<const Derived&>(*this); }

    /// Returns the derived translator.
    Derived& derived() { return static_cast<Derived&>(*this); }

    // Matching ids.

    /// Returns the expected id expression or throws an exception.
    const id_expr*
    expect_id(const expr* e)
    {
      if (const id_expr* id = as<id_expr>(e))
        return id;
      throw_expected_term(e, "identifier");
    }

    /// Matches any id-expression, saving the symbol as `ret`.
    void
    match(const expr* e, symbol** ret)
    {
      const id_expr* id = expect_id(e);
      *ret = id->id;
    }

    /// Matches any id-expression having the value `str`.
    void
    match(const expr* e, const char* str)
    {
      const id_expr* id = expect_id(e);
      if (*id->id != str)
        throw_unexpected_id(id);
    }

    // Matching ints.
    
    /// Returns the expected integer expression.
    const int_expr*
    expect_int(const expr* e)
    {
      if (const int_expr* num = as<int_expr>(e))
        return num;
      throw_expected_term(e, "integer");
    }

    /// Matches any integer expression, saving the result as `ret`.
    void
    match(const expr* e, int* ret)
    {
      const int_expr* num = expect_int(e);
      *ret = num->val;
    }

    /// Matches any id-expression having the value `val`.
    void
    match(const expr* e, int val)
    {
      const int_expr* num = expect_int(e);
      if (num->val != val)
        throw_expected_value(num, val);
    }

    // List matching.

    /// Get's the nth element of a list.
    const expr*
    get(const list_expr* list, int n)
    {
      if (list->exprs.size() < n)
        throw std::runtime_error("no matching s-expression");
      return list->exprs[n];
    }

    /// Matches the concrete syntax `(... str ...)` in the nth position.
    void
    match(const list_expr* list, int n, const char* str)
    {
      match(list->exprs[n], str);
      // match(get(list, n), str);
    }

    /// Matches the concrete syntax `(... id ...)` in th enth position,
    /// saving the result as `ret`.
    void
    match(const list_expr* list, int n, symbol** ret)
    {
      match(get(list, n), ret);
    }
  
    /// Matches the concrete syntax `(... num ...)` in the nth position,
    /// saving `num` in `ret`.
    void
    match(const list_expr* list, int n, int* ret)
    {
      match(get(list, n), ret);
    }

    void
    match(const list_expr* list, int n, int val)
    {
      match(get(list, n), val);
    }

    /// The base case for the `match` function.
    template<int N>
    void
    match_recurse(const list_expr* e)
    { }

    /// The recursive list matcher.
    template<int N, typename T, typename... Args>
    void
    match_recurse(const list_expr* list, T&& arg, Args&&... args)
    {
      derived().match(list, N - sizeof...(Args) - 1, std::forward<T>(arg));
      return match_recurse<N>(list, std::forward<Args>(args)...);
    }

    template<typename... Args>
    void
    match_list(const list_expr* list, Args&&... args)
    {
      return match_recurse<sizeof...(Args)>(list, std::forward<Args>(args)...);
    }
  };

} // namespace sexpr
