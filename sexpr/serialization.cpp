#include "serialization.hpp"
#include "syntax.hpp"

#include <fstream>

namespace sexpr
{
  void
  write_section_table(const section_table& tab, output_record& rec)
  {
    // Write out a table of contents. It has this form:
    //
    //    toc-size
    //    section-1-offset
    //    section-2-offset
    //    ...
    //    section-n-offset
    //
    // We don't need to store section lengths; that's inherent in the
    // encoding of each section.
    int base = 4 + tab.size() * 4; // toc-size
    rec.os.write_int(tab.size());
    for (const cc::byte_seq& sec : tab) {
      rec.os.write_int(base);
      base += sec.size();
    }

    // Each section is simply appended after the TOC.
    for (const cc::byte_seq& sec : tab)
      rec.os.write_bytes(sec);
  }

  void
  bytecode_writer::operator()(const expr* e, const std::string& f)
  {
    // Serialize the expression first.
    write_expr(e);

    // Serialize the unique'd symbols.
    //
    // NOTE: Deserialization requires a single-pass over the table to
    // rebuild the index.
    output_record sym_rec;
    sym_rec.os.write_int(syms.seq.size());
    for (const cc::symbol* s : syms.seq)
      sym_rec.os.write_string(*s);

    /// Build a list of sections for the final serialization.
    section_table tab {
      sym_rec.bytes,
      exprs.bytes
    };

    // Generate the final byte encoding.
    output_record out;
    write_section_table(tab, out);

    // Write to the given file.
    std::ofstream ofs(f, std::ios::out | std::ios::binary);
    const char* ptr = reinterpret_cast<const char*>(out.bytes.data());
    ofs.write(ptr, out.bytes.size());
  }

  /// Generate a unique entry for the symbol `s` and write a reference
  /// in the expression stream.
  void 
  bytecode_writer::write_symbol(const cc::symbol* s)
  {
    int ref = syms.add(s);
    exprs.os.write_int(ref);
  }

  /// Write an expression `e` to bytecode.
  void
  bytecode_writer::write_expr(const expr* e)
  {
    exprs.os.write_int(get_expr_kind(e));
    
    switch (get_expr_kind(e)) {
      case ek_nil:
        return write_nil_expr(cast<nil_expr>(e));
      case ek_id:
        return write_id_expr(cast<id_expr>(e));
      case ek_int:
        return write_int_expr(cast<int_expr>(e));
      case ek_str:
        return write_str_expr(cast<str_expr>(e));
      case ek_list:
        return write_list_expr(cast<list_expr>(e));
    }
  }

  void
  bytecode_writer::write_nil_expr(const nil_expr* e)
  {
    // Nothing to do.
  }

  void
  bytecode_writer::write_id_expr(const id_expr* e)
  {
    write_symbol(e->id);
  }

  void
  bytecode_writer::write_int_expr(const int_expr* e)
  {
    exprs.os.write_int(e->val);
  }

  void
  bytecode_writer::write_str_expr(const str_expr* e)
  {
    write_symbol(e->val);
  }

  void
  bytecode_writer::write_list_expr(const list_expr* e)
  {
    exprs.os.write_int(e->exprs.size());
    for (const expr* x : e->exprs)
      write_expr(x);
  }

} // namespace sexpr
