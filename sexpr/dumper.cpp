#include "dumper.hpp"
#include "syntax.hpp"

#include <iostream>

namespace sexpr
{
  void
  dumper::dump_expr(const expr* e)
  {
    switch (get_expr_kind(e)) {
      case ek_nil:
        return dump_nil_expr(cast<nil_expr>(e));
      case ek_id:
        return dump_id_expr(cast<id_expr>(e));
      case ek_int:
        return dump_int_expr(cast<int_expr>(e));
      case ek_str:
        return dump_str_expr(cast<str_expr>(e));
      case ek_list:
        return dump_list_expr(cast<list_expr>(e));
      default:
        break;
    }
    throw std::logic_error("invlaid expr");
  }

  void
  dumper::dump_nil_expr(const nil_expr* e)
  {
    dump_guard g(*this, e);
  }

  void
  dumper::dump_id_expr(const id_expr* e)
  {
    dump_guard g(*this, e);
    os << ' ' << *e->id;
  }

  void
  dumper::dump_int_expr(const int_expr* e)
  {
    dump_guard g(*this, e);
    os << ' ' << e->val;
  }

  void
  dumper::dump_str_expr(const str_expr* e)
  {
    dump_guard g(*this, e);
    os << ' ' << *e->val;
  }

  void
  dumper::dump_list_expr(const list_expr* e)
  {
    dump_guard g(*this, e, false);
    indent();
    print_newline();
    const expr_seq& exprs = e->exprs;
    for (auto iter = exprs.begin(); iter != exprs.end(); ++iter)
      dump_expr(*iter);
    undent();
  }

  dumper::dump_guard::dump_guard(dumper& d, const expr* e, bool nl)
    : cc::dumper::dump_guard(d, e, get_expr_name(e), nl)
  { }

  void 
  dump(const expr* e)
  {
    dumper d(std::cerr);
    d(e);
  }

} // namespace sexpr
