#include "parser.hpp"
#include "context.hpp"
#include "dumper.hpp"

#include <cctype>
#include <iostream>
#include <sstream>

namespace sexpr
{  
  [[noreturn]]
  static void
  throw_expected_token(cc::location loc, token_kind k, token tok)
  {
    std::stringstream ss;
    ss << "expected '" << token::get_name(k) << "' but got ";
    if (tok)
      ss << '\'' << tok.get_spelling() << '\'';
    else
      ss << "end-of-input";
    throw syntax_error(loc, ss.str());
  }

  [[noreturn]]
  static void
  throw_unexpected_token(cc::location loc, token tok)
  {
    std::stringstream ss;
    ss << "unexpected ";
    if (tok)
      ss << '\'' << tok.get_spelling() << '\'';
    else
      ss << "end-of-file";
    throw syntax_error(loc, ss.str());
  }

  // Paren balancer.
  struct balanced_parens
  {
    balanced_parens(parser& p)
      : p(p)
    { }

    // Consumes the opening paren.
    void open() 
    { 
      lp = p.match(tk_lparen); 
    }

    // Consumes the closing paren.
    void close() 
    { 
      // FIXME: Trap errors and diagnose the opening paren.
      try {
        rp = p.match(tk_rparen);
      }
      catch (syntax_error& err) {
        std::stringstream ss;
        ss << "left parenthesis is here";
        err.note(lp.get_location(), ss.str());
        throw;
      }
    }

    cc::span get_span() const 
    { 
      return {lp.get_location(), rp.get_location()}; 
    }
    
    parser& p;
    token lp;
    token rp;
  };

  parser::parser(context& cxt, const cc::file& f)
    : cxt(cxt), lex(cxt, f)
  { 
    // Advance to the first token.
    tok = lex();
  }

  expr*
  parser::operator()()
  {
    if (tok)
      return parse_expr();
    else
      return {};
  }

  /// Returns true if the next token matches k.
  bool
  parser::next_token_is(token_kind k)
  {
    return tok.get_kind() == k;
  }

  /// Returns true if the next token does not match k.
  bool
  parser::next_token_is_not(token_kind k)
  {
    return tok.get_kind() != k;
  }

  /// Consumes the lookahead token.
  token
  parser::consume()
  {
    token ret = tok;
    tok = lex();
    return ret;
  }

  /// Consumes the lookahead token if it matches tk. Otherwise, emits an error.
  token
  parser::match(token_kind k)
  {
    if (next_token_is(k))
      return consume();
    throw_expected_token(tok.get_location(), k, tok);
  }

  expr*
  parser::parse_expr()
  {
    switch (tok.get_kind()) {
      case tk_lparen:
        return parse_list();
      case tk_id:
        return parse_identifier();
      case tk_int:
        return parse_integer();
      case tk_str:
        return parse_string();
      default:
        break;
    }
    throw_unexpected_token(tok.get_location(), tok);
  }

  expr*
  parser::parse_list()
  {
    assert(next_token_is(tk_lparen) && "expected '('");
    balanced_parens braces(*this);
    braces.open();
    expr_seq nodes;
    while (next_token_is_not(tk_rparen))
      nodes.push_back(parse_expr());
    braces.close();
    if (nodes.empty())
      return cxt.make_nil_expr(braces.get_span());
    else
      return cxt.make_list_expr(braces.get_span(), std::move(nodes));
  }

  expr*
  parser::parse_identifier()
  {
    assert(next_token_is(tk_id) && "expected identifier");
    token id = consume();
    return cxt.make_id_expr(id.get_span(), id.get_symbol());
  }

  expr*
  parser::parse_integer()
  {
    assert(next_token_is(tk_int) && "expected integer");
    token num = consume();
    return cxt.make_int_expr(num.get_span(), num.get_integer());
  }

  expr*
  parser::parse_string()
  {
    assert(next_token_is(tk_str) && "expected string");
    token str = consume();
    return cxt.make_str_expr(str.get_span(), str.get_symbol());
  }

} // namespace sexpr
