#include "token.hpp"

#include <iostream>

namespace sexpr
{
  const char*
  token::get_name(int k)
  {
    switch (k) {
      case tk_eof: return "eof";
      case tk_lparen: return "lparen";
      case tk_rparen: return "rparen";
      case tk_id: return "id";
      case tk_int: return "int";
      default:
        throw std::logic_error("invalid token kind");
    }
  }

  /// The end location is computed from the token class and attribute.
  cc::location
  token::get_end_location() const
  {
    switch (kind) {
      case tk_eof:
        // Abstract tokens have no width.
        return loc;
      
      case tk_lparen:
      case tk_rparen:
        // Tokens of 1 character length.
        return loc.get_next(1);

      case tk_id:
      case tk_int:
        // Tokens of variable length.
        return loc.get_next(sym->size());
      
      default:
        // Fall through for error.
        break;
    }
    throw std::logic_error("invalid token kind");
  }

  /// The spelling of a token depends on its kind.
  const char*
  token::get_spelling() const
  {
    switch (kind) {
      case tk_eof:
        return "<end-of-file>";
      case tk_lparen:
        return "(";
      case tk_rparen:
        return ")";
      case tk_id:
      case tk_int:
        return sym->c_str();
      default:
        break;
    }
    throw std::logic_error("invlaid token kind");
  }

  std::ostream&
  operator<<(std::ostream& os, const token& tok)
  {
    os << '<' << tok.get_name();
    switch (tok.get_kind()) {
      default:
        break;
      case tk_id:
      case tk_int:
      case tk_str:
        os << ':' << ' ' << tok.get_string();
    }
    os << '>';
    return os;
  }
} // namespace sexpr
