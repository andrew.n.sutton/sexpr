#pragma once

#include <cc/location.hpp>
#include <cc/node.hpp>
#include <cc/symbol.hpp>

#include <cassert>
#include <list>
#include <vector>

namespace sexpr
{
  // Imports.
  using cc::is;
  using cc::as;
  using cc::cast;

  using cc::symbol;

// -------------------------------------------------------------------------- //
// Expressions

  enum expr_kind {
    ek_nil,
    ek_id,
    ek_int,
    ek_str,
    ek_list
  };

  /// Returns the the textual name of the node.
  /// This is primarily used for debugging and diagnostics.
  const char* get_expr_name(expr_kind k);

  struct nil_expr;
  struct id_expr;
  struct int_expr;
  struct str_expr;
  struct list_expr;

  /// The set of nodes in the language.
  struct expr : cc::node
  {
    expr(expr_kind k, cc::span locs) 
      : cc::node(k, locs) { }

    void dump() const;
  };

  /// A sequence of nodes.
  using expr_seq = std::vector<expr*>;

  /// Represents the nil value `()`.
  struct nil_expr : expr
  {
    nil_expr(cc::span locs) 
      : expr(ek_nil, locs) { }
  };

  /// Represents an identifier e.g., `x`.
  struct id_expr : expr
  {
    id_expr(cc::span locs, cc::symbol* id) 
      : expr(ek_id, locs), id(id) { }
    
    cc::symbol* id;
  };

  /// Represents an integer value e.g., `3`.
  struct int_expr : expr
  {
    int_expr(cc::span locs, int n) 
      : expr(ek_int, locs), val(n) { }
    
    int val;
  };

  /// Represents an string value e.g., `"foo"`.
  struct str_expr : expr
  {
    str_expr(cc::span locs, cc::symbol* s) 
      : expr(ek_str, locs), val(s) { }
    
    cc::symbol* val;
  };
  
  /// Represents a nested sequence of expressions.
  struct list_expr : expr
  {
    list_expr(cc::span locs, const expr_seq& es) 
      : expr(ek_list, locs), exprs(es) { }
    
    list_expr(cc::span locs, expr_seq&& es) 
      : expr(ek_list, locs), exprs(std::move(es)) { }
    
    expr_seq exprs;
  };

// -------------------------------------------------------------------------- //
// Term name

  /// Returns the kind of s-expression.
  inline expr_kind
  get_expr_kind(const expr* e)
  {
    return static_cast<expr_kind>(e->kind);
  }

  /// Returns a textual representation of the expression name.
  /// This is primarily used for debugging.
  inline const char*
  get_expr_name(const expr* e)
  {
    return get_expr_name(get_expr_kind(e));
  }

} // namespace sexpr

namespace cc
{
// -------------------------------------------------------------------------- //
// Node conversion
//
// Specializations of node info to support casting, etc.

  template<>
  struct node_info<sexpr::nil_expr>
  {
    static bool
    has_kind(const node* n) { return get_node_kind(n) == sexpr::ek_nil; }
  };

  template<>
  struct node_info<sexpr::id_expr>
  {
    static bool 
    has_kind(const node* n) { return get_node_kind(n) == sexpr::ek_id; }
  };

  template<>
  struct node_info<sexpr::int_expr>
  {
    static bool
    has_kind(const node* n) { return get_node_kind(n) == sexpr::ek_int; }
  };

  template<>
  struct node_info<sexpr::str_expr>
  {
    static bool
    has_kind(const node* n) { return get_node_kind(n) == sexpr::ek_str; }
  };

  template<>
  struct node_info<sexpr::list_expr>
  {
    static bool
    has_kind(const node* n) { return get_node_kind(n) == sexpr::ek_list; }
  };

} // namespace cc
