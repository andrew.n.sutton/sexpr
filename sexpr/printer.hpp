#pragma once

#include <cc/print.hpp>

namespace sexpr
{
  class expr;
  class nil_expr;
  class id_expr;
  class int_expr;
  class list_expr;
  class lang_expr;

  // The printer is responsible for pretty printing a program.
  class printer : public cc::printer
  {
  public:
    using cc::printer::printer;

    using cc::printer::print;
    
    void print(const expr* e);

  protected:
    void print_expr(const expr* e);
    void print_nil_expr(const nil_expr* e);
    void print_id_expr(const id_expr* e);
    void print_int_expr(const int_expr* e);
    void print_list_expr(const list_expr* e);
  };

  std::ostream& operator<<(std::ostream& os, const expr* e);

} // namespace sexpr
