#include "context.hpp"
#include "syntax.hpp"

namespace sexpr
{
  context::context(cc::diagnostic_manager& diags, 
                   cc::input_manager& in, 
                   cc::symbol_table& syms)
    : diags(diags), input(in), syms(syms)
  { }

  cc::symbol*
  context::get_symbol(const char* str)
  {
    return syms.get(str);
  }

  cc::symbol*
  context::get_symbol(const std::string& str)
  {
    return syms.get(str);
  }

  expr*
  context::make_nil_expr(cc::span locs)
  {
    return nils.make(locs);
  }

  expr*
  context::make_id_expr(cc::span locs, cc::symbol* sym)
  {
    return ids.make(locs, sym);
  }

  expr*
  context::make_int_expr(cc::span locs, int val)
  {
    return ints.make(locs, val);
  }

  expr*
  context::make_str_expr(cc::span locs, cc::symbol* sym)
  {
    return strs.make(locs, sym);
  }

  expr*
  context::make_list_expr(cc::span locs, expr_seq&& nodes)
  {
    assert(!nodes.empty() && "empty node list");
    return lists.make(locs, std::move(nodes));
  }

} // namesapce sexpr
