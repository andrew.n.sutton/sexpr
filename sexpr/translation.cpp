#include "translation.hpp"

#include <sstream>

namespace sexpr
{
  static bool
  is_vowel(char c) 
  {
    switch (c) {
      case 'a':
      case 'e':
      case 'i':
      case 'o':
      case 'u':
        return true;
      default:
        return false;
    }
  }

  // TODO: Consider adding exceptional cases, or specializing the input
  // for known subsets of nouns (e.g., node names).
  static const char*
  get_article(const char* noun)
  {
    if (is_vowel(noun[0]))
      return "an";
    else
      return "a";
  }

  [[noreturn]]
  void
  throw_unexpected_term(const expr* e)
  {
    std::stringstream ss;
    ss << "unexpected " << get_expr_name(e);
    throw translation_error(get_location(e), ss.str());
  }

  [[noreturn]]
  void 
  throw_expected_term(const expr* e, const char* what)
  {
    const char* got = get_expr_name(e);
    std::stringstream ss;
    ss << "expected " << get_article(what) << ' ' << what << ", "
       << "but got " << get_article(got) << ' ' << got;
    throw translation_error(get_location(e), ss.str());
  }

  [[noreturn]]
  void 
  throw_unexpected_id(const id_expr* e)
  {
    std::stringstream ss;
    ss << "unexpected identifier '" << *e->id << '\'';
    throw translation_error(get_location(e), ss.str());
  }

  [[noreturn]]
  void 
  throw_expected_id(const id_expr* e, const char* what)
  {
    std::stringstream ss;
    ss << "expected " << get_article(what) << ' ' << what << ", "
       << "but got '" << *e->id << '\'';
    throw translation_error(get_location(e), ss.str());
  }

  [[noreturn]]
  void 
  throw_expected_value(const int_expr* e, int what)
  {
    std::stringstream ss;
    ss << "expected " << what << ", " << "but got '" << e->val << '\'';
    throw translation_error(get_location(e), ss.str());
  }

  [[noreturn]]
  void 
  throw_expected_id(const expr* e)
  {
    throw_expected_term(e, "identifier");
  }

  [[noreturn]]
  static void 
  throw_list_size_mismatch(const list_expr* e, int k) 
  {
    std::stringstream ss;
    ss << "too " << (e->exprs.size() < k ? "few" : "many")
       << " list elements; expected " << k;
    throw translation_error(get_location(e), ss.str());
  }

  [[noreturn]]
  static void 
  throw_list_size_underflow(const list_expr* e, int k) 
  {
    std::stringstream ss;
    ss << "too few list elements; expected at least " << k;
    throw translation_error(get_location(e), ss.str());
  }

} // namespace sexpr
